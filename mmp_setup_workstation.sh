
SCRIPT_DIR=$(pwd)
PORTAL_PATH=$SCRIPT_DIR
COMPILED_PATH=$PORTAL_PATH/../public

hugo --destination=$COMPILED_PATH

cd $COMPILED_PATH/_marApp-data
rm -f data-*.xml data-*.json data-*.zip
#ln -s MarRef_*.xml data-marref.xml
#ln -s MarDB_*.xml data-mardb.xml
#ln -s MarCat_*.xml data-marcat.xml
ln -s MarRef_*.json data-marref.json
ln -s MarDB_*.json data-mardb.json
ln -s MarCat_*.json data-marcat.json
ln -s MarRef_*.json.zip data-marref.json.zip
ln -s MarDB_*.json.zip data-mardb.json.zip
ln -s MarCat_*.json.zip data-marcat.json.zip
#ln -s MarRef_*.xml.zip data-marref.xml.zip
#ln -s MarDB_*.xml.zip data-mardb.xml.zip
#ln -s MarCat_*.xml.zip data-marcat.xml.zip

cd $COMPILED_PATH/databases/marref
mkdir -p data
cd data
rm -f mar.xsd data.xml data.json data.json.zip data.xml.zip
ln -s ../../../_marApp-data/mar.xsd
#ln -s ../../../_marApp-data/data-marref.xml data.xml
ln -s ../../../_marApp-data/data-marref.json data.json
ln -s ../../../_marApp-data/endpoints.json
ln -s ../../../_marApp-data/data-marref.json.zip data.json.zip
#ln -s ../../../_marApp-data/data-marref.xml.zip data.xml.zip

cd $COMPILED_PATH/databases/mardb
mkdir -p data
cd data
rm -f mar.xsd data.xml data.json data.json.zip data.xml.zip
ln -s ../../../_marApp-data/mar.xsd
#ln -s ../../../_marApp-data/data-mardb.xml data.xml
ln -s ../../../_marApp-data/data-mardb.json data.json
ln -s ../../../_marApp-data/endpoints.json
ln -s ../../../_marApp-data/data-mardb.json.zip data.json.zip
#ln -s ../../../_marApp-data/data-mardb.xml.zip data.xml.zip

cd $COMPILED_PATH/databases/marcat
mkdir -p data
cd data
rm -f mar.xsd data.xml data.json data.json.zip data.xml.zip
ln -s ../../../_marApp-data/mar.xsd
#ln -s ../../../_marApp-data/data-marcat.xml data.xml
ln -s ../../../_marApp-data/data-marcat.json data.json
ln -s ../../../_marApp-data/endpoints.json
ln -s ../../../_marApp-data/data-marcat.json.zip data.json.zip
#ln -s ../../../_marApp-data/data-marref.xml.zip data.xml.zip
