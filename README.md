
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------


# MARINE METAGENOMICS PORTAL (MMP)


## General information:
- Web address: https://mmp.sfb.uit.no/
- Portal email: mmp@uit.no
- New content can be added / old content can be modified via Admin panel (requires username/password):
https://mmp.sfb.uit.no/admin/
Content examples: webpages text, XSD-scheme and XML-databases for MarApp, download files.
- MarRef, MarDB, MarCat pages are implemented by *MAR database frontend* (MarApp):
https://gitlab.com/uit-sfb/mar-frontend  
- Database downloads are stored here: https://s1.sfb.uit.no/public/mar/
- The handling of emails is implemented by the *NewpanGO*'s Mailer:
https://gitlab.com/uit-sfb/newpango


## Frameworks used in MMP development:
- Hugo, static site generator: https://gohugo.io/
- Caddy, web server with automatic HTTPS: https://caddyserver.com/


## Structure / important locations:
- /content/_marApp-data/*: XSD-scheme and XML-databases currently used by the MarApp.
- /content/*: content data, webpages' text data.
- /static/*: static website data, such as css, images, fonts, MarApp.
- /layouts/*: html-like webpages' code files.
- /_bin/*: binary files used for running the portal.
    - Hugo v0.20.7.
    - Caddy v0.10.2.
- /Caddyfile: the Caddy configuration file.
- /mmp_launch.sh: the script that prepares required symlinks, launches and re-generates the website, periodically pull-pushes the source files.
- /mmp_stop.sh: the script that kills the portal-related processes.
- /mmp_PIDs: temporary file, not in repo, appears only when the portal was launched with "mmp_launch.sh" script. Contains the PIDs of the portal-related processes and is used by the "mmp_stop.sh" script.
- /../mmp-admin: not in repo, contains credentials for the portal admin panel, is created manually according to the "Installing MMP" instructions.

## Requirements for the MMP server:

- Linux (the portal is currently running on CentOS 7).
- Git is installed and prepared for the read-write access to the MMP repository (this repository).
- Website certificates for Caddy are already in place (~/.caddy).


## Installing MMP:

1. SSH to the portal server (mmp.sfb.uit.no) as MMP-user.  
Run:  
cd ~  
git clone git@gitlab.com:uit-sfb/MarineMetagenomicPortal.git  
ln -s MarineMetagenomicPortal/\_bin bin  
ln -s MarineMetagenomicPortal/content/\_marApp-data/ marApp-data  

2. As MMP-user:  
cd ~  
Where the MarineMetagenomicPortal folder is now located.  
Create file "mmp-admin", open it and paste the MMP admin username on the first line and password on the second line.  
Save and close the file.  

3. Now SSH as root.  
Run:  
sudo setcap CAP_NET_BIND_SERVICE=+eip /home/mmp/MarineMetagenomicPortal/\_bin/caddy  
(where "mmp" is the name of the MMP-user)

Ready. Can start using the portal.


## Launching MMP:

SSH into the VM as MMP-user.  

Run:  
cd ~/MarineMetagenomicPortal  
nohup ./mmp_launch.sh &  

The portal is up. 
The portal processes are: "sh" (the "mmp_launch.sh" process) and "caddy".
To see them among the other mmp-user processes, run:
top -U mmp

In case if there is a need to run only Caddy, without the script, then run:  
cd ~/MarineMetagenomicPortal  
nohup \_bin/caddy &  


## Stopping MMP:

Run (given that the portal was launched with "mmp_launch.sh" script):  
cd ~/MarineMetagenomicPortal  
./mmp_stop.sh  


## Update MMP code, content and DBs:

#### Automatic code generation and git pull/push.
When MMP is launched using the mmp_launch.sh script:  
- If developer updates the code in the repository, it will be automatically pulled within the given period.
- If new content was added/modified via the Admin panel, it will be pushed to the repository within the given period, and generated (online) portal will be updated within the given period.

#### Adding/Modifying content and portal files:
- Login to: https://mmp.sfb.uit.no/admin/
- You can see a file system with the root being the "content" folder.
- To modify existing webpage text content:
    - Open the corresponding "{Webpage Name}.md" file.
    - Make your changes.
    - Save the file - press Ctrl+S or the diskette button in the top right.
- To create a new Events/News text content file:
    - Go to Events/News folder.
    - Open "_template.txt" file, copy all its text, close the file.
    - Press "+" botton in the bottom right, create new content file called "{Some News Or Event Name}.md". The "md" extension is important!
    - Open the created file, paste the copied template contents, save and close the file.
    - Open the created file again, now meta-data fields (date, title, etc.) are visible. Now ready to start editing.
    - The text pasted from "_template.txt" has some examples of how to format text, it can be removed when no longer needed.
    - Don't forget to save, when the editing is finished.
- To add download files, just upload them to the "_download_files" folder (use the Upload button in the top right, or simply drug-n-drop the file).

#### Updating the MarApp databases:
- Go to the "/content/_marApp-data" folder.
- To update the XSD-scheme, simply replace the "mar.xsd" file with the new XSD file.
- To update the XML-databases:
    - Delete the old "MarXXX_yyyy-mm-dd.xml" file.
    - Paste the latest XML file.
    - Edit the "data-marXXX.xml" symlink, so that it points to the new file.
    
#### Updating MarApp:
- Given that you already have the generated updated MarApp files - "bundle.js" and "bundle.js.map".
- Go to "/static/marApp" folder and replace these 2 files.


## For development:

1. Git-clone the repository to desired location on the dev machine:  
git clone git@gitlab.com:uit-sfb/MarineMetagenomicPortal.git  
Then run the following commands in terminal.
2. cd MarineMetagenomicPortal  
3. _bin/hugo server  
or just:  
hugo server  
if Hugo is installed on the dev machine.  
Now the portal should be accessible on http://localhost:1313  
Can start modifying the code.  
Each time a source file is modified, the portal is refreshed in the browser.


------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------


