
SCRIPT_DIR="`( cd \"\`dirname \"$0\"\`\" && pwd )`"
PORTAL_PATH=$SCRIPT_DIR
MMP_PIDS_FILE=$SCRIPT_DIR/mmp_PIDs

if [[ ! -f $MMP_PIDS_FILE ]] ; then
    echo "The required file $MMP_PIDS_FILE doesn't exist!"
    exit
fi

kill $(sed -n '1 p' $MMP_PIDS_FILE)
kill $(sed -n '2 p' $MMP_PIDS_FILE)

echo "MMP processes killed: $(sed -n '1 p' $MMP_PIDS_FILE), $(sed -n '2 p' $MMP_PIDS_FILE)"

