+++
date = "0001-01-01T00:00:00"
private = true
title = "MarDB"
type = "databasesCol2"
+++

MarDB includes all non-complete sequenced marine microbial genomes regardless of level of completeness.
<br>
The current version contain 13237 partly curated records for prokaryotic genomes.
