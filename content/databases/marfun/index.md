+++
date = "0001-01-01T00:00:00"
layout = "single"
title = "MarFun"
type = "marfun"
+++

MarFun is a manually curated marine fungi genome database. The current version consists of 28 entries.
