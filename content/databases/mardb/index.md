+++
date = "0001-01-01T00:00:00"
layout = "single"
title = "MarDB"
type = "mardb"
+++

MarDB includes all non-complete marine microbial genomes regardless of level of completeness. Each entry contains 120 metadata fields including information about sampling environment or host, organism and taxonomy, phenotype, pathogenicity, assembly and annotation.
<br>
The current version contain 13237 partly curated genomes.
