+++
date = "0001-01-01T00:00:00"
layout = "single"
title = "SalDB"
type = "saldb"
+++

SalDB is a salmon specific database of genome sequenced prokaryotes representing the microbiota of fishes found in the taxonomic family of Salmonidae.
The current version holds 348 entries.
