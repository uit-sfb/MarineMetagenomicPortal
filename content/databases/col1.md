+++
date = "0001-01-01T00:00:00"
private = true
title = "MarRef"
type = "databasesCol1"
+++

MarRef is a manually curated marine microbial genome database that contains complete genomes.
<br>
The current version contain 970 prokaryote genomes.
