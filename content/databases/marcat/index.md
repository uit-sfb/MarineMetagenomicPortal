+++
date = "0001-01-01T00:00:00"
layout = "single"
title = "MarCat"
type = "marcat"
+++

MarCat is a gene (protein) catalogue of uncultivable and cultivable marine genes and proteins derived from metagenomics samples.
The data is produced by META-pipe, and the current version has 1227 records from projects like the Tara Ocean expedition and Ocean Sampling Day (OSD).
