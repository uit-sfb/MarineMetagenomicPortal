+++
date = "0001-01-01T00:00:00"
private = true
title = "MarCat"
type = "databasesCol3"
+++

MarCat is a gene (protein) catalogue of uncultivable and cultivable marine microorganisms derived from metagenomics samples. The data is produced by META-pipe, and the current version contain 1227 samples.