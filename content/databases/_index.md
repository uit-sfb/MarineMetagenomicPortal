+++
date = "0001-01-01T00:00:00"
title = "The MAR Databases"
type = "index"
weight = 1.0
+++

The MAR databases are a collection of richly annotated and manually curated contextual (metadata) and sequence databases. The contextual data can be accessed by browsing, searching or filtering, while the sequence data through BLAST. All data can be downloaded.