---
date: 2016-11-03T14:46:00
layout: single
title: MarRef
type: marref
...
---

MarRef is a manually curated marine microbial reference genome database that 
contains completely sequenced genomes. Each entry contains 120 metadata fields including information about sampling environment or host, organism and taxonomy, phenotype, pathogenicity, assembly and annotation information. The current version contain 970 genomes.
