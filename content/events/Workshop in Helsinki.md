+++
date = "2017-06-04T00:00:00"
title = "Metagenomics Workshop in Helsinki"
+++

Together with ELIXIR-FI, we gave a workshop in [Metagenomics data analysis](https://www.csc.fi/web/training/-/metagenomics) at CSC, Helsinki 3-6 April 2017
Course [materials](https://kannu.csc.fi/s/lR2kSA5vN2delFq) and [videos](https://www.youtube.com/playlist?list=PLjiXAZO27elBa5zGKCpwvRXxx-kF52Iuf) are now available]

See also the [article on the Metagenomics course](https://www.elixir-europe.org/news/supporting-metagenomics-training) written by Eija Korpelainen (ELIXIR Finland) and Premysl Velek (ELIXIR Hub)
