---
date: 2017-08-15T00:00:00
title: ELIXIR Innovation and SME Forum
...
---

We are invited to give a talk about marine metagenomics use case at the ELIXIR Innovation and SME Forum: Data-driven innovation in food, nutrition and the microbiome, taking place at the KVS - Brussels City Theatre, Brussels, October 9-10, 2017