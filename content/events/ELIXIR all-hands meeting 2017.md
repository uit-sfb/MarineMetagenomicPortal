+++
date = "2017-01-21T00:00:00+02:00"
title = "ELIXIR all-hands meeting 2017"
+++

We will give talks, present posters, and a co-host a workshop at the [ELIXIR all-hands meeting](https://www.elixir-europe.org/events/elixir-all-hands-2017) in Rome 21-23 March 2017.

