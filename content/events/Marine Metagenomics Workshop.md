+++
date = "2018-03-17T00:00:00"
title = "Workshop in Marine Metagenomics, Oeiras, Portugal"
+++

[A practical workshop in marine metagenomics](http://elixir-portugal.org/event/elixir-excelerate-workshop-marine-metagenomics)  will be held during 7-11 May 2018, in Oeiras, Portugal hosted by the Portuguese ELIXIR Node in cooperation with ELIXIR Norway.
