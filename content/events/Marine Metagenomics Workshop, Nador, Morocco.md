+++
date = "2018-01-10T00:00:00"
title = "Marine Metagenomics Workshop, Nador, Morocco."
+++

We are co-organizing the [First Marine Microbiome workshop Metagenomics and Bioinformatics for Biodiversity](http://medicalintelligence.org/marmicrobiome2018/) in Nador, Morocco, 5-9 Feb. 2018.
