+++
date = "2018-03-16T00:00:00"
title = "12th CeBiTec Symposium Big Data in Medicine and Biotechnology"
+++

The Marine Metagenomics Use Case will be presented at the [12th CeBiTec Symposium
Big Data in Medicine and Biotechnology](https://www.cebitec.uni-bielefeld.de/index.php/events/conferences/525-2018-03-019-12th-cebitec-symposium-big-data-in-medicine-and-biotechnology), the Center for Interdisciplinary Research (ZiF), Bielefeld University,
March 19 - 21, 2018
