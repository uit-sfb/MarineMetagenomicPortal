---
date: 2017-07-10T00:00:00
title: ISMB-ECCB 2017
...
---

We will give at talk about the [The ELIXIR Marine Metagenomics Use Case](https://www.iscb.org/ismbeccb2017-program/special-sessions#SST02) at the SST02 special sessions: Critical Assessment of Metagenome Interpretation (CAMI), [ISMB-ECCB 2017](https://www.iscb.org/ismbeccb2017) meeting in Prague.
