+++
title = " ! the name had to be hard-coded in the code ! "
type = "indexCol2"
date = "2016-12-22T14:09:37+01:00"
private = true
+++

META-pipe is a complete workflow for the analysis of marine metagenomic data.
It provides assembly of high-throughput sequence data, functional annotation of predicted genes, and taxonomic profiling.
META-pipe is not released as an ELIXIR service yet. For now you may use the [NeLS META-pipe service](https://nels.bioinfo.no/).