+++
date = "0001-01-01T00:00:00"
private = true
title = "MAR Download"
type = "indexCol4"
+++

Download gives easy and open access to the MAR contextual data, sequence data and other resources generated from the curated MAR databases for use in different tools such as MAPseq, Kaiju and Kraken
