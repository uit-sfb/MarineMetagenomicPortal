+++
date = "0001-01-01T00:00:00"
private = true
title = "MAR Databases"
type = "indexCol1"
+++

The MAR (MARine) databases are richly annotated and manually curated contextual and sequence databases.
[MarRef](/databases/marref) contains only completed, finished marine prokaryotic genomes.
[MarDB](/databases/mardb) includes all non-complete marine prokaryotic genomes regardless of level of completeness.
[SalDB](/databases/saldb) is a manually curated database for salmonide associated genomes.
[MarFun](/databases/marfun) is a manually curated marine fungi genome database.
