---
date: 2016-12-22T14:09:00
title: Marine Metagenomics Portal
type: index
weight: 1
...
---

The mission of Marine Metagenomics Portal (MMP) is to provide the marine scientific community with

high-quality curated and freely accessible microbial genomics and metagenomics resources.