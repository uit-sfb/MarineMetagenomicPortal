+++
date = "0001-01-01T00:00:00"
private = true
title = "Contextual Data"
type = "downloadsCol1"
+++

The contextual MAR data in TSV, XML and JSON format is available from the links below. For more info see Help page.
<br>
<br>
__MarRef v5.0__ &nbsp; &nbsp; &nbsp;
<br>
[TSV](https://public.sfb.uit.no/MarRef/Metadatabase/Current.tsv), [JSON](https://public.sfb.uit.no/MarRef/Metadatabase/Current.json), [XML](https://public.sfb.uit.no/MarRef/Metadatabase/Current.xml)
<br>
Click [here](https://public.sfb.uit.no/MarRef/Metadatabase/) for earlier versions.
<br>
__MarDB v5.0__ &nbsp; &nbsp; &nbsp;
<br>
[TSV](https://public.sfb.uit.no/MarDB/Metadatabase/Current.tsv), [JSON](https://public.sfb.uit.no/MarDB/Metadatabase/Current.json), [XML](https://public.sfb.uit.no/MarDB/Metadatabase/Current.xml) 
<br>
Click [here](https://public.sfb.uit.no/MarDB/Metadatabase/) for earlier versions.
<br>
__MarFun v2.0__ &nbsp; &nbsp; &nbsp;
<br>
[TSV](https://public.sfb.uit.no/MarFun/Metadatabase/Current.tsv), [JSON](https://public.sfb.uit.no/MarFun/Metadatabase/Current.json), [XML](https://public.sfb.uit.no/MarFun/Metadatabase/Current.xml)
<br>
Click [here](https://public.sfb.uit.no/MarFun/Metadatabase/) for earlier versions.
<br>
__MarCat v2.0__ &nbsp; &nbsp; &nbsp;
<br>
[TSV](https://public.sfb.uit.no/MarCat/Metadatabase/Current.tsv), [JSON](https://public.sfb.uit.no/MarCat/Metadatabase/Current.json), [XML](https://public.sfb.uit.no/MarCat/Metadatabase/Current.xml)
<br>
Click [here](https://public.sfb.uit.no/MarCat/Metadatabase/) for earlier versions.
