+++
date = "0001-01-01T00:00:00"
private = true
title = "Blast Sequence Data"
type = "downloadsCol2"
+++

All MAR sequence data including assemblies, CDS nucleotides and proteins in FASTA format is available for download. For more info see Help page.
<br>
<br>
__MarRef v5.0__ &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
<br>
[Assembly](https://public.sfb.uit.no/MarRef/BLAST/assembly/), 
[CDS Nucleotides](https://public.sfb.uit.no/MarRef/BLAST/nucleotides/), 
[CDS Proteins](https://public.sfb.uit.no/MarRef/BLAST/proteins/)
<br>
__MarDB v5.0__ &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
<br>
[Assembly](https://public.sfb.uit.no/MarDB/BLAST/assembly/), 
[CDS Nucleotides](https://public.sfb.uit.no/MarDB/BLAST/nucleotides/), 
[CDS Proteins](https://public.sfb.uit.no/MarDB/BLAST/proteins/)
<br>
__MarFun v2.0__ &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
<br>
[Assembly](https://public.sfb.uit.no/MarFun/BLAST/assembly/), 
[CDS Nucleotides](https://public.sfb.uit.no/MarFun/BLAST/nucleotides/), 
[CDS Proteins](https://public.sfb.uit.no/MarFun/BLAST/proteins/)
<br>
__MarCat v2.0__ &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
<br>
[CDS Proteins](https://s1.sfb.uit.no/public/mar/MarCat/BLAST/proteins/)
