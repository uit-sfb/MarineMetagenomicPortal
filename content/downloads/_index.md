+++
date = "0001-01-01T00:00:00"
title = "The MAR Download"
type = "index"
weight = 1.0
+++

The MAR databases and resources can be downloaded using the MAR browser. All data including contextual, sequence and other sequence data resources are open and freely available.