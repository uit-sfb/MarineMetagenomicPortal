+++
date = "0001-01-01T00:00:00"
private = true
title = "Other Data Resources"
type = "downloadsCol3"
+++

Links to data, lists etc. generated from the curated MAR databases for use in tools such as MAPseq, Kaiju and Kraken. For more info see the Help page.
<br>
<br>
[SILVA MAR](https://public.sfb.uit.no/Resources/SILVAmar/)
<br>
[Kaiju MAR](https://public.sfb.uit.no/Resources/kaiju/)
<br>
[Kraken MAR](https://public.sfb.uit.no/Resources/kraken/)
<br>
[MAPseq MAR](https://public.sfb.uit.no/Resources/mapseq/)
<br>
[ITSoneDB MAR](https://public.sfb.uit.no/Resources/ITSoneMarine/)
