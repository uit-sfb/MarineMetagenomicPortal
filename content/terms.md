---
date: 2016-09-09T00:00:00
title: Terms Of Use
type: terms
...
---

__The MAR databases__
<Br>

We have chosen to apply the  [Creative Commons Attribution-NoDerivs License](https://creativecommons.org/licenses/by-nd/3.0/) to all copyrightable parts of our databases. This means that you are free to copy, distribute, display and make commercial use of these databases, provided you give us credit. However, if you intend to distribute a modified version of one of our databases, you must ask us for permission first. Any genetic information is provided for research, educational and informational purposes only. 


__META-pipe__
<Br>
META-pipe is a free available online service. However, Third party softwares implemented in META-pipe will have their own individual license agreements. It is the responsibility of users to ensure that they do not violate with the confidentiality of these licence agreements. META-pipe utilise public databases contributed by the community who remain the data owners. The original data may be subject to rights claimed by third parties.It is the responsibility of users to ensure that their exploitation of the data does not infringe any of the rights of such third parties.
<Br>

__Privacy policy__
<Br>
 In compliance with the EU GDPR regulation we like to inform that we use cookies to collect information about visitors (IP addresses), date/time visited, page visited, referring website, etc. for site usage statistics and to improve our websites and services. No personal data is stored when using our websites. For more information about the EU General Data Protection Regulation please visit the following website [gdpr.org](https://gdpr.eu/).Feedback and data submitted through this website will never be shared with any other third parties.
<Br>

__Disclaimer__
<Br>
We make no warranties regarding the correctness of the data, and disclaim liability for damages resulting from its use. We cannot provide unrestricted permission regarding the use of the data, as some data may be covered by patents or other rights.

__ELIXIR Terms of Use of the ELIXIR Services__
<Br>
In addition we follow [ELIXIR Terms of Use of the ELIXIR Services](https://www.elixir-europe.org/legal/terms-of-use).
