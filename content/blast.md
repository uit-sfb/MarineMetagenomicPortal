+++
date = "0001-01-01T00:00:00"
layout = "single"
title = "BLAST"
type = "blast"
+++

The BLAST (Basic Local Alignment Search Tool) finds local similarity between sequences, which can be used to infer functional and evolutionary relationships between sequences as well as help identify members of gene families.