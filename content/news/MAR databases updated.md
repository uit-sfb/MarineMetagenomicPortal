+++
date = "2017-08-15T00:00:00"
title = "MAR databases updated"
+++

We have released an updated version of the MAR databases. MarRef contains now 612 complete genomes, MarDB 3726 incomplete genomes, and MarCat contains 1227 metagenomics samples.