+++
date = "2019-06-20T00:00:00"
title = "New release of MarRef and MarDB"
+++

We are pleased to announce the first release of MarFun, a database for marine fungi. The first version contains 21 manually curated genomes. 
MarRef and MarDB have been updated to version four, which contains 945 and 12972 entries respectively. MarDB contains now more MAGs (Metagenome Assembled Genomes) than WGSs (Whole Shotgun Sequenced) and SAGs (Single Amplified Genomes) together, with 7053, 4902 and 1017 entries, respectively. 
The quality of each genome is now provided using the CheckM output completeness (%), contamination (%) and QS (Quality Score = completeness - 5 x contamination).
