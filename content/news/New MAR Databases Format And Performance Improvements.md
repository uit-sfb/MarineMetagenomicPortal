+++
date = "2018-03-23T14:00:00"
title = "New MAR Databases Format And Performance Improvements"
+++

The MAR databases are now available in JSON format.
The MAR databases web application ([MarRef](https://mmp.sfb.uit.no/databases/marref/#/), [MarDB](https://mmp.sfb.uit.no/databases/mardb/#/), [MarCat](https://mmp.sfb.uit.no/databases/marcat/#/)) now uses databases in JSON format instead of XML, this made it load up to 2.5 times faster.
The MAR databases can be downloaded [here](https://s1.sfb.uit.no/public/mar/).
