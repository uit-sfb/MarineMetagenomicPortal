+++
date = "2017-11-09T00:00:00"
title = "MAR databases published"
+++

The MAR databases was published 2nd Nov 2017 in Nucleic Acid Research (NAR) as a paper for the NAR Database issue 2018. In the paper “The MAR databases: development and implementation of databases specific for marine metagenomics” we describe how we build the databases and the content. The open access paper can be downloaded from [NAR](https://doi.org/10.1093/nar/gkx1036).
