+++
date = "2019-01-19T00:00:00"
title = "MarRef and MarDB v3 available"
+++

MarRef and MarDB v3 is now available for browsing, blasting and downloading. MarRef and MarDB contains now 839 complete and 10869 non-complete genomes, respectively. We have improved the curation of many attributes by implemented Controlled Vocabulary (CV) e.g. for "Analysis project type" contain only the terms Whole genome sequencing (WGS), Metagenome assembled genome (MAG) and Single amplified genome (SAG). We have included a "MAR Download" page to ease the access to the contextual and sequence data in addition to microbial genomics and metagenomics resources generated from the MAR databases.
