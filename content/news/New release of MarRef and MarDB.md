+++
date = "2018-04-12T00:00:00"
title = "New release of MarRef and MarDB"
+++

We are pleased to announce the release of MarRef and MarDB v2. We have added 174 and 4942 new genomes to MarRef and MarDB, respectively, bringing the total number of MAR genomes to approximately 9500. The redesigned database schema and web user interface support several new features including the implementation of predicted secondary metabolites obtained from antiSMASH and new attributes for a better description of MAGs (Metagenome Assembled Genomes). We have also improved the web application - the loading time of the databases has been substantially reduced.
