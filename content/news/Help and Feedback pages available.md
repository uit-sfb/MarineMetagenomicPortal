---
date: 2017-07-15T00:00:00
title: HELP and FEEDBACK
...
---

We have now included a HELP page to help our end-users to use the services provided by Marine Metagenomics Portal (MMP). In addition, we also included a FEEDBACK form. We hope the MMP users will take advantage of using the feedback, helping the MMP team to build a better working environment and improve our services.
