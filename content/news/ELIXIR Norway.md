---
date: 2017-07-01T00:00:00+02:00
title: New funding awarded to ELIXIR Norway
---

ELIXIR Norway is among 19 national research infrastructures selected for funding by the [Research Council of Norway](https://www.forskningsradet.no/en/Newsarticle/NOK_1_billion_for_research_infrastructure/1254027850326). For more information visit [ELIXIR](https://www.elixir-europe.org/news/new-funding-awarded-elixir-norway) and [ELIXIR Norway](https://www.elixir-europe.org/about-us/who-we-are/nodes/norway).