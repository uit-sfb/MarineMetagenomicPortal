+++
date = "2018-03-23T00:00:00"
title = "MarRef and MarDB available in gbff"
+++

MarRef and MarDB is now available in gbff format. For downloading please follow the links below;
[MarRef](https://s1.sfb.uit.no/public/mar/MarRef/gbff_files/),
[MarDB](https://s1.sfb.uit.no/public/mar/MarDB/gbff_files/)
