+++
date = "2016-11-17T00:00:00+02:00"
title = "META-pipe is an important demonstrator"
+++

META-pipe is an important demonstrator for the ELIXIR compute platform services as demonstrated in the [webinar](https://www.youtube.com/watch?v=42cNWSmle4E).
