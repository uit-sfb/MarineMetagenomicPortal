+++
date = "2016-04-14T00:00:00+02:00"
title = "The design, implementation and evaluation of META-pipe 1.0"
+++

The design, implementation and evaluation of META-pipe 1.0 is described in this [paper](http://arxiv.org/abs/1604.04103).
