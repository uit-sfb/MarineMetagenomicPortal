---
date: 2017-07-25T00:00:00
title: ELIXIR Core Data Resources and Deposition Databases
...
---

ELIXIR published today the initial lists of ELIXIR Core Data Resources and Deposition Databases. The lists can be [here](https://www.elixir-europe.org/news/core-data-resources)