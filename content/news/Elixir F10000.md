+++
date = "2017-01-24T00:01:00+02:00"
title = "Our paper in the Elixir F10000"
+++

Our [paper](http://dx.doi.org/10.12688/f1000research.10443.1) in the Elixir F10000 channel summarizes the results from the ELIXIR pilot action “Marine metagenomics – towards user centric services”.

