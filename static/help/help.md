## **Overview**
The mission of Marine Metagenomics Portal (MMP) is to provide the marine scientific community with
high-quality curated and freely accessible microbial genomics and metagenomics resources.

### Marine metagenomics


The use of metagenomics (study of genetic material recovered directly from environmental samples) in the marine domain is increasing rapidly. Today marine metagenomics is used in a range of scientific and industry applications from monitoring pollution and recycling water systems to disease survalliance and development of functional feed for the aquaculture industry as shown in the figure below. 


[<img src="/images/overview_metagenomics.png" width="80%">](/images/overview_metagenomics.png)

Metagenomics has the potential to provide unprecedented insight into the structure and function of heterogeneous communities of microorganisms and their vast biodiversity. However, in order to expand the potential further for the research community and biotech industry the metagenomics methodologies need to overcome a number of challenges related to standardization, development of relevant databases and bioinformatics tools. New and emerging sequencing technologies, integration and curation of metadata gives an extra burden to the development of future databases and tools. Here we describe the Marine Metagenomics Portal, a portal dedicated to marine research and innovation.


### The Marine Metagenomics Portal

The primary access to the resources or services is provided through the MMP main menu, available at the top of the home page. 
<br>
<br>
[<img src="/images/mmp_overview2.png" width="70%">](/images/mmp_overview1.png)
<br>

#### **The main menu options:**

- **Services** - Links to services provided by MMP. Currently, META-pipe - a pipeline for taxonomic classification and functional annotation of metagenomics sample, MarRef, MarDB and MarCat - the marine genome/metagenome reference databases, BLAST and Download. 
- **Documentation** - Links to pages containing additional information such as Publications, Source Code, Workshops and Webinars.
- **Community** - Links to pages containing additional information conserning the portal such as News, Twitter and Events. 
- **Help** - This page.
- **Contact** - How to find and contact the MMP team.
- **Helpdesk** - Provide fast and efficient helpdesk support.
<br>

## **Services**

### The MAR Databases
In order to establish the marine resources, we have chosen to define **“marine microbial biome”** as *“An aquatic microbial biome comprises of microbial communities from open-oceans, costal and protected habitats up to the high water mark with salinity from 0.5 ppt as in estuaries (brackish water) environments to above 100 ppt as in sea ice brine. The biome also includes marine microbial communities obtained from marine species associated with these habitats”* 

We have chosen to include soil samples from sandy shores, intertidal zone, salt marshes (coastal salt marsh or a tidal marsh), mudflats and estuaries, in addition to habitats such as seawater saltern, sea ice brine, black smokers (hydrothermal vents) where the salinity can we extreme high or low compared to seawater. Microorganisms associated with marine species, as defined by the World Register of Marine Species, WoRMS , have also been defined as marine. This includes microorganisms associated with or causing diseases in marine animals or plants for example coral, shellfish and fish.

- **MarRef** is a database for completely sequenced marine prokaryotic genomes.  
- **MarDB** includes all sequenced marine prokaryotic genomes regardless the level of completeness.
- **MarFun** is a database for marine fungi genomes.
- **MarCat** contains DNA, gene and protein records are generated from marine metagenomics samples using META-pipe.
<br>
<br>
[<img src="/images/Mar_databases_010819.png" width="70%">](/images/Mar_databases_010819.png)
<br>

### The MAR Databases Landing Page
In the The MAR Databases Landing Page, available samples (which has been populated with GPS location) from the databases are displayed in a google map. The samples of MarRef, MarDB and MarCat are marked with Blue, Orange or Purple, respectively.  All samples can be activated. When activated, a box will be populated with sample name and the description of the sample. Triggering the link will bring the visitor directly to the contextual data (metadata) for the respective genome/metagenome.
<br>
<br>
[<img src="/images/MAR2.png" width="70%">](/images/MAR2.png)
<br>
### Genome Browser Page
In the Genome Browser Page the visitor can browse through all entries in the databases. Each genome in MarRef and MarDB is described by 106 metadata fields (attributes) and each metagenome sample with 55 attributes in MarCat. The genome metadata is descriptive data about single genomes. In MarRef and MarDB these attributes are organized into the following seven broad information categories: Summary, Organism and taxon, Isolate, Host and pathogenicity, Assembly, Annotation and Phenotype. In MarCatthe following categories are used: Summary, Isolate, Sampling and Assembly.
<br>
<br>
[<img src="/images/MAR3.png" width="70%">](/images/MAR3.png)
<br>
#### **Search**
You may search the marine reference databases, namely by:

- Searching for an organism name
- Searching by the database accession or ID
- Searching for any word
- Using the filter function to build a query restricted by multiple fields  

#### **Filter**
There are several filter entries to be visible in the table based on the most important record attribute, such as environmental ontologies (biome, feature and material) and taxonomy (phylum, order and genus)
Advanced filtering allows the site visitor to:

- add one or more filters
- refine current filters by adding new filters or removing already applied filters
- combine search and filtering
- remove all filters and launch a new search.


#### **Display**
The search/filtered results will be listed in a table. Summary of the metadata will be shown when click the row of the sample. Clicking “Expand all” on the left side of the table to show more available metadata for that particular genome.  

<br>
[<img src="/images/MAR4.png" width="70%">](/images/MAR4.png)
<br>

#### **Outlinks**
From each entry we provide links to a number sequence and contextual databases such as INSDC, UniProt, SILVA, BioProject, BioSample, NCBI taxon, including litterature databases such as PubMed and Europe PMC.
<br>

#### **Secondary metabolites**
Secondary metabolites has been predicted using antiSMASH. For result for each genome is available from the "Secondary metabolites" page or from the "Browse" page using the Search function.
<br>
### BLAST
The BLAST (Basic Local Alignment Search Tool) finds local similarity between sequences, which can be used to infer functional and evolutionary relationships between sequences as well as help identify members of gene families. The BLAST provides search on all genes and protein coding sequences from the MAR databases.
<br>
<br>

#### **BLAST Search Page**
<br>
[<img src="/images/MAR6.png" width="70%">](/images/MAR6.png)
<br>

#### **BLAST Search**

#### **1. Loading a sequence** 
Cut and paste or or drag-and-drop a protein or nucleotide sequence (raw sequence or fasta format) into the into the form. 
<br>
#### **2. Select appropriate BLAST method**
The user interface automagically figures out the appropriate BLAST method (e.g. BLASTP, TBLASTN for protein sequences and BLASTN, BLASTX for nucleotide) for the given query and selected databases.
<br>
#### **3. Selecting database(s)** 
MMP provides both nucleotide and protein sequence databases for MarRef, MarDB and MarCat.
<br>
#### **4. Advanced BLAST parameters** 
Checking more advanced Options by activating the question mark on the right side of the query box.
<br>
#### **5. Submitting the BLAST job**
Once the sequence has been uploaded, the method and database selected, the BLAST parameters adjusted, the job can be started by activating the BLAST button.
<br>
#### **6. Results**
The output of BLAST consists of a list of hits with the corresponding e-value, and a set of the traditional pairwise alignments were the target sequence can be viewed and downloaded. From the pairwise alignment the visitor can also retrieve information of the organism/metagenome sample in the MAR databases by opening the mmp button. 
<br>
<br>
[<img src="/images/MAR7.png" width="70%">](/images/MAR7.png)
<br>

<br>
[<img src="/images/MAR8.png" width="70%">](/images/MAR8.png)
<br>

In MarRef and MarDB information about the targets sequences can be obtained by opening the NCBI button. For MarCat, the marine metagenomics gene catalogue, target information can be obtained from other databases such as UniProt, InterPro and Brenda. Output from the BLAST search can be downloaded in FASTA, extensible markup language files (XML) or tab-separated files (TSV) format. 
<br>

### META-pipe
META-pipe is a complete workflow for the analysis of marine metagenomic data. It provides assembly of high-throughput sequence data, functional annotation of predicted genes, and taxonomic profiling. META-pipe is not released as an ELIXIR service yet. For now you may use the NeLS META-pipe service. 
<br>

### The MAR Download
The Download page gives free access to the contextual data, sequence and other microbial genomics and metagenomics resources generated from the MAR databases.
<br>
<br>
[<img src="/images/download_1.png" width="70%">](/images/download_1.png)
<br>
#### **Contextual Data**
The MAR contextual databases, MarRef, MarDN and MarCat, can be separately downloaded as tsv, XML or JSON format, which which are available for the current and prior release versions. MarRef and MarDB contains 121 attributes (metadata fields), out of which 30 fields are represented by controlled vocabularies (CV) and the remaining are free text or numeric fields. These 121 metadata fields include information about sampling environment, the organism and taxonomy, phenotype, pathogenicity, secondary metabolites, assembly and annotation. MarCat contains 55 metadata fields.
<br>
<br>
#### **BLAST Sequence Data**
From the "BLAST Sequence Data" page one can download the concatinated assembly, nucleotide and protein data of MarRef, MarDB and MarCat (only protein) in FASTA format which can be used to generate the MAR BLAST databases. The FASTA files has to be converted by makeblastdb before they can be used from the blast+ package.
<br>
<br>
#### **Other Data Resources**
In "Other Data Resources" you can find data, lists etc. generated from the curated MAR databases for use in different tools such as MAPseq, Kaiju, Kraken etc. Be aware that some of the data e.g. the KrakenMAR is huge (>200 GB). In each of the folder you will find a README.txt explaining the content in the folder. Please read carefully.
<br>
<br>
#### *SILVA MAR* 
SILVA MAR is the SSU rRNA marine subset pertaining to the environment as defined at The Marine Metagenomics Protal (mmp.sfb.uit.no). Further details regarding build and usage are available in the repository https://github.com/emrobe/SILVAmar This repository also holds scripts to build and use MAPseq with SILVA MAR.
<br>
<br>
#### *Kaiju MAR*
Kaiju Mar is a marine specific integration with the taxonomic classification software Kaiju based on the Mar databases. This intergration has two relevant git repositories:
<br>
"https://github.com/emrobe/kaiju"
This repository is used for staging. When a new version of the Mar databases is launched, neccessary fixes and updates are pushed to this repository first
<br>
"https://github.com/bioinformatics-centre/kaiju"
KaijuMar reference build and usage gets merged with the original git repository for Kaiju after some time with a pull request.
Please read the README.txt carefully.
<br>
<br>
#### *Kraken MAR*
The Kraken MAR folder hosts static files needed to build a KrakenMar kmer reference. See the How-to for usage and build. Please read the README.txt carefully.
<br>
<br>
#### *MAPseq MAR*
MapSeq includes the funcitonality to use a custom database as a basis for taxonomic classification. As a marine specific resource, we provide a mapseq formated fasta and tax file based on entries from the Mar databases. Please read the README.txt carefully.
<br>
<br>
#### *ITSoneDB MAR*
This folder hosts an ITSone marine sequence subset pertaining to fungi (NCBI taxid: 4751). Original sequences are provided by IBIOM and is refined using the World Register of Marine Species (WoRMS) The original subset is obtainable via http://itsonedb.cloud.ba.infn.it/ITS1Marine/. Please read the README.txt carefully.
<br>

## **Documentation**
Short decription on who we build and implemented the MAR databases, META-pipe in addition to design documents, source code repositories, webinars and our scientific papers. 
<br>

## **Community**
The latest News, Events and Tweets related to the MMP activities.
<br>

## **Help**
This page
<br>


## **Contact**
Here you will find the mail and visiting address, in addition to the e-mail address.
<br>


## **Helpdesk**
Send an e-mail (to [mmp@uit.no](mailto:mmp@uit.no)) if you have any difficulties, remark or just suggestions to improve the content of the portal and services provided. The MMP team will respond as soon as possible. 
You should include your full name and your affiliation in your request.
<br>

## **Recommended System Requirements for MMP**
The following web browsers have been tested:  

| Browser           | Version(s) | OS                | Additional information                                                                                    |
|-------------------|------------|-------------------|-----------------------------------------------------------------------------------------------------------|
| Chrome            | 60-63      | Windows, Mac OS X | <span style="color:green">Great performance in MAR Databases Web Application.</span>                      |
| Opera             | 7, 4X-6X   | Windows           | <span style="color:green">Great performance in MAR Databases Web Application.</span>                      |
| Vivaldi           | 1.5-2.X    | Windows           | <span style="color:green">Great performance in MAR Databases Web Application.</span>                      |
| Safari            | 10.1       | Mac OS X          | <span style="color:green">Great performance in MAR Databases Web Application.</span>                      |
| Firefox           | 5X-6X      | Windows, Mac OS X | <span style="color:#907000">Good performance in MAR Databases Web Application.</span>                     |
| Microsoft Edge    | 40         | Windows           | <span style="color:#C04000">Bad performance in MAR Databases Web Application.</span>                      |
| Internet Explorer | 11         | Windows           | <span style="color:red">Not supported: MAR Databases Web Application, Audio-Captcha in “Helpdesk”.</span> |

<br>