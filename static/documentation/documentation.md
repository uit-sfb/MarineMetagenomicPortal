### **The MMP Team**
The Marine Metagenomics Portal was developed as a part of the ELIXIR EXCELERATE project and made online in 2017. The Portal is maintained by [The Center for Bioinformatics (SfB)](https://en.uit.no/forskning/forskningsgrupper/gruppe?p_document_id=347053) at the UiT The Arctic University of Norway. SfB is hosting the UiT node of [ELIXIR Norway](https://www.elixir-europe.org/about-us/who-we-are/nodes/norway).
Below you will find short decription on who we build and implemented the MAR databases, META-pipe in addition to design documents, source code repositories, webinars and our scientific papers.   

<br>

### **Building the MAR Contextual Databases**
#### **Data collection**
The MarRef, MarDb, SalDB, MarFun and MarCat contextual databases are built by compiling data from a number of public available sequence, taxonomy and literature databases in a semi-automatic fashion. Other databases or resources such as bacterial diversity and culture collections databases, web mapping service and ontology databases are used extensively for curation of metadata as shown in the figure below.  
<br>
<br>
[<img src="/images/Workflow_v.2.2.png" width="70%">](/images/Genome Metadata Sources.png)
<br>

The contextual MAR databases, supports the International community-driven standards of the Genomics Standards Consortium and is fully compliant with its recommendations for Minimum Information about any (x) Sequence (MIxS) standards, including MIGS (Minimum Information about a Genome Sequence), MISAG (Minimum information about a single amplified genome)  and MIMAG (a metagenome-assembled genome) of bacteria and archaea and MIMS (Minimal Information of Metagenome sequence). The databases have also included the proposed standards for provenance of analysis developed in the ELIXIR-EXCELERATE project ([The metagenomic data life-cycle: standards and best practices](https://doi.org/10.1093/gigascience/gix047).

MarRef, MarDB and SalDB contain in total 970, 13237 and 293 records, respectively, with 120 metadata fields, out of which 40 fields are represented by CV and the remaining are free text or numeric fields. These 120 metadata fields include information about sampling environment, the organism and taxonomy, phenotype, pathogenicity, secondary metabolites, assembly and annotation. MarFun, a database of marine fungi, contains at the moment 21 records. MarCat represents a catalogue of uncultivable (and cultivable) full-length genes (proteins) derived from marine metagenomic samples based on the Marine projects in [EBI metagenomics](https://www.ebi.ac.uk/metagenomics/) and contains 1227 records, including samples from the Tara Ocean expedition (248 records) and Ocean Sampling Day (150 records). Each record contains 55 metadata fields.  
<br>

#### **Curation, refinement and validation**
The curation process is essential to ensure that all manually annotated entries are handled in a consistent manner. For the MAR databases, the imported data files are compiled, converted to tab separated value files (TAB) format and imported into base, a full-featured desktop database front end, provided by provided by [LibreOffice](https://no.libreoffice.org/). Each record undergoes a critical review of experimental and predicted data as well as manual verification of information from the literature. The records are continuously updated as new information becomes available.  
[OpenRefine](http://openrefine.org/) was used for refining the metadata fields by cleaning, trimming of leading and trailing whitespace, transforming data from one format into another and extending it with web services and external data. Several validation tool has been developet to ensure consistency e.g. the validator which we use to convert the tab separated value files (TSV) to extensible markup language files (XML/JSON) and from TSV to XML/JSON to link the source TSV curation databases to the JSON database. The validator defines a set of rules for the conversion – warnings and errors during conversion are reported. The whole curation process is shown in the figure below.
<br>

[<img src="/images/Curation workflow 010819.png" width="50%">](/images/Curation workflow 010819.png)

<br>



### **The MAR Sequence Databases**
#### **The MarRef, MarDB and SalDB sequence databases**
The MarRef and MarDB sequence databases are primarily built on gene, protein and genome sequences obtained from the [Prokaryotic RefSeq Genomes](https://www.ncbi.nlm.nih.gov/refseq/about/prokaryotes/) database. All archaeal and bacterial genomes in RefSeq have been annotated using the NCBI’s [Prokaryotic Genome Automatic Annotation Pipeline](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5001611/), PGAAP. However, ∼20% of all records in MarDB did not have any RefSeq entry with PGAAP annotations. Circumventing the lack of gene and protein information of these genomes, annotation was performed on pre-assembled sequences using [Prokka](https://www.ncbi.nlm.nih.gov/pubmed/24642063), a command line software tool, for annotation of prokaryotic genomes.  
<br>

#### **The MarFun sequence database**
The MarFun sequence database is based upon genomes downloaded from ENA.
<br>
<br>

#### **The MarCat sequence databases**
Metagenomic sequence reads were downloaded from ENA and annotated using [META-pipe](https://arxiv.org/abs/1604.04103). In short, sequencing reads were merged, filtered and assembled using MEGAHIT. From the resulting contigs, full-length CDSs were predicted using MetaGeneAnnotator and functionally assigned using a compilation of results from BLAST against UniRef, Priam and InterProScan5.  
<br>

#### **Databases derived from the MarRef, MarDB and SalDB**
From MarRef and MarDB, several sequence databases has been generated to enhance taxonomic classification and functional assignment of metagenomics samples. SILVAmar is a subset database of the [SILVA database ](https://www.arb-silva.de/), which provides comprehensive, quality checked of aligned small (16S SSU) ribosomal RNA (rRNA) sequences for bacteria and archaea, based on the curated entries in MarRef and MarDB. A 16S rRNA database for [MAPseq](https://github.com/jfmrod/MAPseq), a set of fast and accurate sequence read classification tools designed to assign taxonomy and OTU classifications, based on MarRef and MarDB has also been generated. The Kaijumar protein sequence database has been implemented in [Kaiju](http://kaiju.binf.ku.dk/), a program for sensitive taxonomic classification of high-throughput sequencing reads from metagenomic experiments.
The METAmar sequence database, was established to increase the functional assignment of protein coding sequences (CDS`s) in marine metagenomics samples, consists of all predicted genes in MarRef and MarDB and contain approx. 40 million entries.  
<br>

#### **Secondary metabolites**
Secondary metabolites was predicted by using the bacterial version (v4.1.0) of [antiSMASH 4](https://doi.org/10.1093/nar/gkx319). The antiSMASH results can be assessed from MarRef, MarDB and SalDB either by using the link to the antiSMASH results for each record or by searching all records in the Browse meny in MarRef or MarDB. For more info regarding the antiSMASH output please follow this [link](https://docs.antismash.secondarymetabolites.org/using_antismash/#antismash-output). All records has also been mapped to [ChEBI](https://www.ebi.ac.uk/chebi/) and [ChEMBL](https://www.ebi.ac.uk/chembl/)  
<br>

#### **Update**
The MAR databases is updated bi-anually, usually in January/February and August/September.  

<br>

### **META-pipe**
META-pipe is a pipeline for annotation and analysis of marine metagenomics samples, which provides insight into phylogenetic diversity, metabolic and functional potential of environmental communities. META-pipe consists of four parts; prosessing of reads (merging, filtering, 16S rRNA extraction and assembly), taxonomic classification (using reads against RefSeq and MAR databases, and predicted 16S rRNA), binning and functional assignment of predicted coding sequences (CDSs) as shown in the figure below.

[<img src="/images/META-pipe_V2.png" width="70%">](/images/META-pipe_V2.png)

<br>

META-pipe is used to generate MarCat, in short, sequencing reads were merged, filtered and assembled using [SeqPrep](https://github.com/jstjohn/SeqPrep), [Trimmomatic](https://doi.org/10.1093/bioinformatics/btu170), [HmmSearch](https://www.mankier.com/1/hmmsearch) and [MEGAHIT](https://doi.org/10.1093/bioinformatics/btv033). From the resulting contigs, full-length CDSs are predicted using [MetaGeneAnnotator](https://doi.org/10.1093/dnares/dsn027) and functionally assigned using a compilation of results from [Diamond](https://doi.org/10.1038/nmeth.3176) against [UniRef](https://doi.org/10.1093/bioinformatics/btu739), MarRef, output from [Priam](https://doi.org/10.1093/nar/gkg847) and [InterProScan5](https://doi.org/10.1093/bioinformatics/btu031). By using META-pipe for gene prediction and functional assignment, allowed us to generate a consistent catalog across the datasets in MarCat.

<br>

### **Terms of use**
We have chosen to apply the Creative Commons Attribution-NoDerivs License to all copyrightable parts of our databases. This means that you are free to copy, distribute, display and make commercial use of these databases, provided you give us credit. However, if you intend to distribute a modified version of one of our databases, you must ask us for permission first. Any genetic information is provided for research, educational and informational purposes only.
META-pipe is an available online service. However, Third party softwares implemented in META-pipe will have their own individual license agreements. It is the responsibility of users to ensure that they do not violate with the confidentiality of these licence agreements. META-pipe utilise public databases contributed by the community who remain the data owners. The original data may be subject to rights claimed by third parties. It is the responsibility of users to ensure that their exploitation of the data does not infringe any of the rights of such third parties. 
Feedback and data submitted through this website will never be shared with any other third parties. We reserve the right to use information about visitors (IP addresses), date/time visited, page visited, referring website, etc. for site usage statistics and to improve our services. 
We make no warranties regarding the correctness of the data, and disclaim liability for damages resulting from its use. We cannot provide unrestricted permission regarding the use of the data, as some data may be covered by patents or other rights.

<br>

### **Funding**
The development of the Marine Metagenomics Portal (MMP) is funded by [ELIXIR](http://www.elixir-europe.org/), the European Union’s Horizon 2020 research and innovation programme under the grant agreement # 676559 [ELIXIR EXCELERATE](https://www.elixir-europe.org/excelerate), the Research Council of Norway [Research Infrastructure program](https://www.forskningsradet.no/prognett-infrastruktur/Home_page/1224697900438) under the grant agreement # 270068 ELIXIR Norway - a distributed infrastructure for the next generation of life science, and not least the [UiT The Arctic University of Norway](https://en.uit.no/startsida). The integration of secondary metabolites are funded by the [EMBRIC](http://www.embric.eu/) project (Grant no.654008).  
It previously received funding from the [Research Council of Norway](https://www.forskningsradet.no/en/Home+page/1177315753906) under the grant agreement # 208481, ELIXIR.NO - a Norwegian ELIXIR node and [UiT The Arctic University of Norway](https://en.uit.no/startsida).  

<br>

### **How to cite**
If you find this Marine Metagenomics Portal useful, please consider citing the following publication:  
MAR databases: Klemetsen T, Raknes IA, Fu J, Agafonov A, Balasundaram SV, Tartari G, Robertsen E, Willassen NP. (2017) [The MAR databases: development and implementation of databases specific for marine metagenomics.](https://doi.org/10.1093/nar/gkx1036)  
META-pipe: Espen Mikal Robertsen, Hubert Denise, Alex Mitchell, Robert D. Finn, Lars Ailo Bongo, Nils Peder Willassen.(2017)
[ELIXIR pilot action: Marine metagenomics - towards a domain specific set of sustainable services](http://dx.doi.org/10.12688/f1000research.10443.1).

<br>

### **Papers**

#### **MAR databases**

1. Klemetsen T, Raknes IA, Fu J, Agafonov A, Balasundaram SV, Tartari G, Robertsen E, Willassen NP. (2017) [The MAR databases: development and implementation of databases specific for marine metagenomics](https://doi.org/10.1093/nar/gkx1036)
2. Hoopen PT, Finn RD, Bongo LA, Corre E, Fosso B, Meyer F, Mitchell A, Pelletier E, Pesole G, Santamaria M, Willassen NP, Cochrane G. (2017) [The metagenomic data life-cycle: standards and best practices](https://www.ncbi.nlm.nih.gov/pubmed/28637310)

#### **META-pipe**

1. Kidane M. Tekle1, Sveinung Gundersen, Kjetil Klepper, Lars Ailo Bongo, Inge Alexander Raknes, Xiaxi Li, Wei Zhang, Christian Andreetta, Teshome Dagne Mulugeta, Matúš Kalaš, Morten B. Rye, Erik Hjerde, Jeevan Karloss Antony Samy, Ghislain Fornous, Abdulrahman Azab, Dag Inge Våge, Eivind Hovig, Nils Peder Willassen, Finn Drabløs, Ståle Nygård, Kjell Petersen, Inge Jonassen. [Norwegian e-Infrastructure for Life Sciences (NeLS)](https://f1000research.com/articles/7-968/) [version 1; referees: 2 approved]. F1000Research 2018, 7(ELIXIR):968 (doi: 10.12688/f1000research.15119.1).
2. Inge Alexander Raknes, Lars Ailo Bongo. [META-pipe Authorization service](https://f1000research.com/articles/7-32) [version 2; referees: 2 approved]. F1000Research 2018, 7(ELIXIR):32 (doi: 10.12688/f1000research.13256.2).
3. Aleksandr Agafonov, Kimmo Mattila, Cuong Duong Tuan, Lars Tiede, Inge Alexander Raknes, Lars Ailo Bongo. [META-pipe cloud setup and execution](https://f1000research.com/articles/6-2060) [version 2; referees: 1 approved, 1 approved with reservations]. F1000Research 2017, 6(ELIXIR):2060(doi: 10.12688/f1000research.13204.1).
4. Espen Mikal Robertsen, Hubert Denise, Alex Mitchell, Robert D. Finn, Lars Ailo Bongo, Nils Peder Willassen.(2017)
[ELIXIR pilot action: Marine metagenomics - towards a domain specific set of sustainable services](http://dx.doi.org/10.12688/f1000research.10443.1).
5. Espen Mikal Robertsen, Tim Kahlke, Inge Alexander Raknes, Edvard Pedersen, Erik Kjærner Semb, Martin Ernstsen, Lars Ailo Bongo, Nils Peder Willassen.(2016) [META-pipe - Pipeline Annotation, Analysis and Visualization of Marine Metagenomic Sequence Data](http://arxiv.org/abs/1604.04103).
6. Inge Alexander Raknes, Bjørn Fjukstad, Lars Ailo Bongo. (2016) [nsroot: Minimalist Process Isolation Tool Implemented With Linux Namespaces](http://arxiv.org/abs/1609.03750).

<br>

### **Reports  / Documents  / Source code**
#### **Reports**

1. [EGI-Engage D6.15 Demonstrator for ELIXIR workflows implemented in the EGI Federated cloud](https://documents.egi.eu/public/ShowDocument?docid=3019). 2017. (describes META-pipe Elixir cloud setup)
2. [The ELIXIR Compute Platform: A Technical Services Roadmap for supporting Life Science Research in Europe](https://docs.google.com/document/d/1gMKFrcbzuN9BSREU1VDnlml-bl6KSOnfyQbJGh20L5s/edit#heading=h.emwdkms5d5x).
2016.


#### **Design documents**

1. META-pipe
2. [Reference databases](http://arxiv.org/abs/1604.04103)
3. [MMG cluster setup](https://docs.google.com/document/d/1ONGUhcmPblRU6vppl-L2qzHVeqSVCO3btgWo-W3IpRQ)
4. [META-pipe authorization server](https://docs.google.com/document/d/1dlq23Q5N9xNLKjgolxug2hTRtI0zuZzHKuX_xfUo7Uo)


#### **Source code  / Projects  / READMEs**

1. [We have a GitLab for our open source repositories](https://gitlab.com/uit-sfb)
2. META-pipe
3. Backend
4. [MMG cluster setup](https://gitlab.com/uit-sfb/METApipe-cPouta-cloud-setup)
5. Authorization server
6. Job manager
7. [Marine Metagenomics Portal](https://gitlab.com/uit-sfb/MarineMetagenomicPortal)

<br>

### **Webinars**

1. Aleksandr Agafonov, Lars Ailo Bongo, Inge Alexander Raknes, et al. ELIXIR Compute platform.
[Webinar](https://www.youtube.com/watch?v=42cNWSmle4E)
and [slides](https://www.elixir-europe.org/system/files/slides_-_elixir_compute_platform_roadmap_webinar_november_2016.pdf),
Elixir, November, 2016.
2. Espen M. Robertsen and Inge Alexander Raknes.
[Update on ELIXIR Pilot Actions launched in 2014: Marine metagenomics: towards user centric services](https://youtu.be/uSsvIZhY8Hs).
Webinar, Elixir, June, 2015.

<br>

### **Theses**

1. Tim Alexander Teige. [Auto scaling framework, simulator, and algorithms for the META-pipe backend](https://munin.uit.no/handle/10037/12898). Master's thesis. Dept. of Computer Science, University of Tromsø. June 2018.
2. Espen Mikal Robertsen. [META-pipe – Distributed Pipeline Analysis of Marine  Metagenomic Sequence Data](https://munin.uit.no/handle/10037/11180). Ph. D. thesis. Dept. of Chemistry. University of Tromsø. February 2017.
3. Edvard Pedersen. [A Data Management Model For Large-Scale Bioinformatics Analysis](https://munin.uit.no/handle/10037/10944). Ph. D. thesis. Dept. of Computer Science, University of Tromsø. September 2016. 
4. Jarl Fagerli. [COMBUSTI/O. Abstractions facilitating parallel execution of programs implementing common I/O patterns in a pipelined fashion as workflows in Spark](http://munin.uit.no/handle/10037/9361). Master thesis. Dept. of Computer Science, University of Tromsø. December 2015.