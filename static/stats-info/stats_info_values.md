
/*
    This is actually a javascript file.
*/

var statsInfoData = [
    {
        value: 2616,    text: "META-pipe jobs"
    },
    {
        value: 72,      text: "META-pipe users"
    },
    {
        value: 91.6,    text: "GB of META-pipe input datasets"
    },
    {
        value: 208.7,   text: "GB of META-pipe output datasets"
    },
    {
        value: 2141,    text: "MMP and META-pipe portals unique visitors"
    },
];

var statsTimer = 4000;

var statsPreText = "Statistics: ";


