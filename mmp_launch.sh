
SCRIPT_DIR="`( cd \"\`dirname \"$0\"\`\" && pwd )`"
PORTAL_PATH=$SCRIPT_DIR
COMPILED_PATH=$PORTAL_PATH/../public
PATH=$PATH:$PORTAL_PATH/_bin
ADMIN_FILE=$PORTAL_PATH/../mmp-admin
MMP_PIDS_FILE=$SCRIPT_DIR/mmp_PIDs
GIT_ITER_SECONDS=300        # should be a multiple of HUGO_ITER_SECONDS
HUGO_ITER_SECONDS=10

echo $$ > $MMP_PIDS_FILE

hugo --destination=$COMPILED_PATH

cd $COMPILED_PATH/_marApp-data
rm -f data-*.xml data-*.json data-*.zip
ln -s MarRef_*.json data-marref.json
ln -s MarDB_*.json data-mardb.json
ln -s MarCat_*.json data-marcat.json
ln -s MarFun_*.json data-marfun.json
ln -s MarRef_*.json.zip data-marref.json.zip
ln -s MarDB_*.json.zip data-mardb.json.zip
ln -s MarCat_*.json.zip data-marcat.json.zip
ln -s MarFun_*.json.zip data-marfun.json.zip
ln -s SalDB_*.json.zip data-saldb.json.zip

cd $COMPILED_PATH/databases/marref
mkdir -p data
cd data
rm -f mar.xsd data.xml data.json data.json.zip data.xml.zip
ln -s ../../../_marApp-data/mar.xsd
ln -s ../../../_marApp-data/endpoints.json
ln -s ../../../_marApp-data/data-marref.json data.json
ln -s ../../../_marApp-data/data-marref.json.zip data.json.zip

cd $COMPILED_PATH/databases/mardb
mkdir -p data
cd data
rm -f mar.xsd data.xml data.json data.json.zip data.xml.zip
ln -s ../../../_marApp-data/mar.xsd
ln -s ../../../_marApp-data/endpoints.json
ln -s ../../../_marApp-data/data-mardb.json data.json
ln -s ../../../_marApp-data/data-mardb.json.zip data.json.zip

cd $COMPILED_PATH/databases/marcat
mkdir -p data
cd data
rm -f mar.xsd data.xml data.json data.json.zip data.xml.zip
ln -s ../../../_marApp-data/mar.xsd
ln -s ../../../_marApp-data/endpoints.json
ln -s ../../../_marApp-data/data-marcat.json data.json
ln -s ../../../_marApp-data/data-marcat.json.zip data.json.zip

cd $COMPILED_PATH/databases/marfun
mkdir -p data
cd data
rm -f mar.xsd data.xml data.json data.json.zip data.xml.zip
ln -s ../../../_marApp-data/mar.xsd
ln -s ../../../_marApp-data/endpoints.json
ln -s ../../../_marApp-data/data-marfun.json data.json
ln -s ../../../_marApp-data/data-marfun.json.zip data.json.zip

cd $COMPILED_PATH/databases/saldb
mkdir -p data
cd data
rm -f mar.xsd data.xml data.json data.json.zip data.xml.zip
ln -s ../../../_marApp-data/mar.xsd
ln -s ../../../_marApp-data/endpoints.json
ln -s ../../../_marApp-data/data-saldb.json data.json
ln -s ../../../_marApp-data/data-saldb.json.zip data.json.zip

cd $PORTAL_PATH

sed -i "/basicauth \/admin/c\\\tbasicauth \/admin $(sed -n '1 p' $ADMIN_FILE) $(sed -n '2 p' $ADMIN_FILE)" Caddyfile
nohup \_bin/caddy & 
echo $! >> $MMP_PIDS_FILE
sleep 3
sed -i "/basicauth \/admin/c\\\tbasicauth \/admin xxxxxxxxxxxxxxxxxxxx xxxxxxxxxxxxxxxxxxxx" Caddyfile

ITER_RELATION=$(($GIT_ITER_SECONDS / $HUGO_ITER_SECONDS))
git config --global user.email mmp@mmp.sfb.uit.no
git config --global push.default simple
while true
do
    echo -e "\n$(date)\GIT_ITER_SECONDS=$GIT_ITER_SECONDS, HUGO_ITER_SECONDS=$HUGO_ITER_SECONDS\n"
    git pull
    for ((i=0; i<$ITER_RELATION; i++)); do
       echo -e "\nIteration $(($i+1)) of $ITER_RELATION\n"
       hugo --destination=$COMPILED_PATH
       echo -e "\nSleeping $HUGO_ITER_SECONDS seconds...\n"
       sleep $HUGO_ITER_SECONDS
    done
    git add -A content
    git reset -- content/*/data.xml
    git reset -- content/*/data.xml.zip
    git reset -- content/*/data.json
    git reset -- content/*/data.json.zip
    git reset -- content/*/mar.xsd
    git reset -- content/_marApp-data/*
    git add -A static
    git commit -m 'AUTOMATIC COMMIT: portal content updated from the web-admin panel'
    git push
    #echo -e "\nSleeping $PERIOD_SECORDS seconds...\n"
    #sleep $GIT_ITER_SECONDS
done

